// sets the scale of the player
void Macro_Scale(entity this)
{
	// determine which factor to apply the scale by
	// the scale factor must range between 0.5 and 1.5
	float scale_wanted_factor = 0;
	if(!IS_PLAYER(this))
	{
		// not a valid player
	}
	else if(autocvar_g_macro_type_botskill && clienttype(this) == CLIENTTYPE_BOT)
	{
		// scale is skill based
		scale_wanted_factor = 0.5 + (0.5 - this.bot_likeprey * 0.5) + (this.bot_likepred * 0.5);
	}
	else
	{
		float healtharmor = (this.health + this.armorvalue) / 2;
		switch(autocvar_g_macro_type)
		{
			case 0:
				// scale is random
				scale_wanted_factor = this.scale ? 0 : 0.5 + random();
				break;
			case 1:
				// scale is health based
				scale_wanted_factor = 0.5 + bound(0, (this.health - autocvar_g_macro_healtharmor_min) / (autocvar_g_macro_healtharmor_max - autocvar_g_macro_healtharmor_min), 1);
				break;
			case 2:
				// scale is armor based
				scale_wanted_factor = 0.5 + bound(0, (this.armorvalue - autocvar_g_macro_healtharmor_min) / (autocvar_g_macro_healtharmor_max - autocvar_g_macro_healtharmor_min), 1);
				break;
			case 3:
				// scale is health and armor based
				scale_wanted_factor = 0.5 + bound(0, (healtharmor - autocvar_g_macro_healtharmor_min) / (autocvar_g_macro_healtharmor_max - autocvar_g_macro_healtharmor_min), 1);
				break;
		}
	}

	// configure the bounding box of the player
	float scale_wanted = pow(scale_wanted_factor, autocvar_g_macro_factor);
	if(scale_wanted && this.scale != scale_wanted)
	{
		// calculate the new bounding box stats
		vector new_pl_min = autocvar_sv_player_mins * scale_wanted;
		vector new_pl_max = autocvar_sv_player_maxs * scale_wanted;
		vector new_pl_crouch_min = autocvar_sv_player_crouch_mins * scale_wanted;
		vector new_pl_crouch_max = autocvar_sv_player_crouch_maxs * scale_wanted;
		vector new_pl_view_ofs = autocvar_sv_player_viewoffset * scale_wanted;
		vector new_pl_crouch_view_ofs = autocvar_sv_player_crouch_viewoffset * scale_wanted;

		// pick which of the new bounding box stats is currently in use
		vector current_pl_min = new_pl_min;
		vector current_pl_max = new_pl_max;
		vector current_pl_view_ofs = new_pl_view_ofs;
		if(IS_DUCKED(this))
		{
			current_pl_min = new_pl_crouch_min;
			current_pl_max = new_pl_crouch_max;
			current_pl_view_ofs = new_pl_crouch_view_ofs;
		}

		// apply a vertical offset to keep the player above the ground, based on the difference between the old and new offset
		float origin_offset_old = current_pl_min.z * this.scale;
		float origin_offset_new = current_pl_min.z * scale_wanted;
		vector origin_offset = this.origin + vec3(0, 0, origin_offset_old - origin_offset_new);

		// if increasing the size of the box would cause the player to get stuck, wait until the player is in an open area before doing so
		tracebox(origin_offset, current_pl_min, current_pl_max, origin_offset, false, this);
		if(!trace_startsolid)
		{
			setsize(this, current_pl_min, current_pl_max);
			setorigin(this, origin_offset);
			this.view_ofs = current_pl_view_ofs;
			this.scale = scale_wanted;
		}
	}

	if(this.scale)
	{
		// set the bounding box and view offset stats
		STAT(PL_MIN, this) = autocvar_sv_player_mins * this.scale;
		STAT(PL_MAX, this) = autocvar_sv_player_maxs * this.scale;
		STAT(PL_CROUCH_MIN, this) = autocvar_sv_player_crouch_mins * this.scale;
		STAT(PL_CROUCH_MAX, this) = autocvar_sv_player_crouch_maxs * this.scale;
		STAT(PL_VIEW_OFS, this) = autocvar_sv_player_viewoffset * this.scale;
		STAT(PL_CROUCH_VIEW_OFS, this) = autocvar_sv_player_crouch_viewoffset * this.scale;

		// scale the exterior weapon entity proportionally with the player
		if(this.exteriorweaponentity)
			this.exteriorweaponentity.scale = 1 / this.scale;
	}

	// apply weight
	// the vore mutator sets this separately, only change it here if that's disabled
	if(!cvar("g_vore"))
	{
		this.gravity = this.trigger_gravity_check ? this.trigger_gravity_check.enemy.gravity : 1;
		this.gravity *= this.scale ? pow(this.scale, autocvar_g_macro_weight) : 1;
	}

	// enforce teleport limitations
	if((this.aiment && this.aiment != world) || this.vehicle)
		this.teleportable = false;
	else if(autocvar_g_macro_teleport_min && this.scale && this.scale < autocvar_g_macro_teleport_min)
		this.teleportable = false;
	else if(autocvar_g_macro_teleport_max && this.scale && this.scale > autocvar_g_macro_teleport_max)
		this.teleportable = false;
	else
		this.teleportable = TELEPORT_NORMAL;
}

// plays footstep sounds
void Macro_Footstep(entity this)
{
	float duckofs = IS_DUCKED(this) ? 0.5 : 1;
	float scaleofs = this.gravity ? min(1, fabs(this.gravity - 1) * duckofs) : 0;
	float shakeofs = this.gravity ? max(0, (this.gravity - 1) * duckofs) : 0;
	float vol = bound(0, VOL_BASE * scaleofs, 1);
	float pitch = this.gravity ? bound(0, pow(this.gravity, -autocvar_g_macro_pitch), 1) : 1;

	if(autocvar_g_footsteps && IS_PLAYER(this) && !IS_DEAD(this) && IS_ONGROUND(this) && (!this.aiment || this.aiment == world) && !this.vehicle && vdist(this.velocity, >, autocvar_sv_maxspeed * 0.4))
	{
		if(time > this.macrostep_finished)
		{
			// play micro and macro footstep sounds
			trace_dphitq3surfaceflags = 0;
			tracebox(this.origin, this.mins, this.maxs, this.origin - '0 0 1', MOVE_NOMONSTERS, this);
			if not(trace_dphitq3surfaceflags & Q3SURFACEFLAG_NOSTEPS)
			{
				if(this.gravity >= 1)
					sound7(this, CH_TRIGGER, SND(MACRO_FOOTSTEP), vol * 1.0, ATTEN_NORM, 100 * pitch, 0);
				else
					sound7(this, CH_TRIGGER, SND(MICRO_FOOTSTEP), vol * 0.5, ATTEN_NORM, 100 * pitch, 0);
			}

			// ground particle effects
			float amount = floor(max(0, this.scale - 1) * EFFECT_AMOUNT_FOOTSTEP);
			if(trace_dphitq3surfaceflags & Q3SURFACEFLAG_NOSTEPS)
				{ /* do nothing */ }
			else if(trace_dphitq3surfaceflags & Q3SURFACEFLAG_METALSTEPS)
				Send_Effect(EFFECT_MACRO_GROUND_METAL, this.origin, '0 0 0', amount);
			else
				Send_Effect(EFFECT_MACRO_GROUND_DIRT, this.origin, '0 0 0', amount);

			// earthquake effect for nearby players when a macro walks by
			if(autocvar_g_macro_quake_step)
			{
				FOREACH_ENTITY_RADIUS(this.origin, autocvar_g_macro_quake_step_radius, it != this && IS_PLAYER(it) && IS_ONGROUND(it), {
					float shake;
					shake = vlen(it.origin - this.origin);
					if(shake)
						shake = 1 - bound(0, shake / autocvar_g_macro_quake_step_radius, 1);
					shake *= shakeofs * autocvar_g_macro_quake_step;

					it.punchvector_x += crandom() * shake;
					it.punchvector_y += crandom() * shake;
					it.punchvector_z += crandom() * shake;
				});
			}

			this.macrostep_finished = time + bound(0.1, (0.25 + random() * 0.1) / pitch, 1); // frequency is based on pitch
		}
	}

	if(IS_PLAYER(this) && this.waterlevel < WATERLEVEL_SWIMMING && !this.ladder_entity)
	{
		if(IS_ONGROUND(this))
		{
			if(!this.last_onground)
			{
				// play micro and macro fall sounds
				trace_dphitq3surfaceflags = 0;
				tracebox(this.origin, this.mins, this.maxs, this.origin - '0 0 1', MOVE_NOMONSTERS, this);
				if not(trace_dphitq3surfaceflags & Q3SURFACEFLAG_NOSTEPS)
				{
					if(this.gravity >= 1)
						sound7(this, CH_TRIGGER, SND(MACRO_HITGROUND), vol * 1.0, ATTEN_NORM, 100 * pitch, 0);
					else
						sound7(this, CH_TRIGGER, SND(MICRO_HITGROUND), vol * 0.5, ATTEN_NORM, 100 * pitch, 0);
				}

				// ground particle effects
				float amount = floor(max(0, this.scale - 1) * EFFECT_AMOUNT_FALL);
				if(trace_dphitq3surfaceflags & Q3SURFACEFLAG_NOSTEPS)
					{ /* do nothing */ }
				else if(trace_dphitq3surfaceflags & Q3SURFACEFLAG_METALSTEPS)
					Send_Effect(EFFECT_MACRO_GROUND_METAL, this.origin, '0 0 0', amount);
				else
					Send_Effect(EFFECT_MACRO_GROUND_DIRT, this.origin, '0 0 0', amount);

				// earthquake effect for nearby players when a macro falls
				if(autocvar_g_macro_quake_fall)
				{
					FOREACH_ENTITY_RADIUS(this.origin, autocvar_g_macro_quake_fall_radius, it != this && IS_PLAYER(it) && IS_ONGROUND(it), {
						float shake;
						shake = vlen(it.origin - this.origin);
						if(shake)
							shake = 1 - bound(0, shake / autocvar_g_macro_quake_fall_radius, 1);
						shake *= shakeofs * autocvar_g_macro_quake_fall;

						it.punchvector_x += crandom() * shake;
						it.punchvector_y += crandom() * shake;
						it.punchvector_z += crandom() * shake;
					});
				}

				this.last_onground = true;
			}
		}
		else
			this.last_onground = false;
	}
}

// --------------------------------
// Hook functions:
// --------------------------------

void MacroMain_OnAdd()
{

}

void MacroMain_OnRemove()
{
	FOREACH_CLIENT(IS_PLAYER(it),
	{
		it.scale = 0;
	});
}

// PlayerPhysics_PostUpdateStats mutator hook
void MacroMain_PlayerPhysics_PostUpdateStats(entity this)
{
	// execute all per-frame functions
	Macro_Scale(this);
	Macro_Footstep(this);
}

// PlayerPhysics_UpdateStats mutator hook, returns float "offset"
float MacroMain_PlayerPhysics_UpdateStats_Offset(entity this)
{
	float wishspeed = 1;
	if(this.scale) // if we are smaller or larger, we run slower or faster
		wishspeed *= pow(this.scale, autocvar_g_macro_movementfactor);
	return wishspeed;
}

// PlayerJump mutator hook, returns float "offset"
float MacroMain_PlayerJump_Offset(entity this)
{
	float wishspeed = 1;
	if(this.scale) // if we are smaller or larger, we run slower or faster
		wishspeed *= pow(this.scale, autocvar_g_macro_movementfactor);
	return wishspeed;
}

// Damage_Calculate mutator hook, returns vector "damage"
vector MacroMain_Damage_Calculate_Damage(entity player_attacker, entity player_target, vector force, float player_damage_take, float player_damage_save, float player_deathtype, float player_damage)
{
	vector newdamage = ('1 0 0' * player_damage_take) + ('0 1 0' * player_damage_save); // x is health, y is armor
	if(autocvar_g_macro_damage && player_target.scale)
		newdamage.x *= pow(player_target.scale, -autocvar_g_macro_damage);
	return newdamage;
}

// CopyBody mutator hook
void MacroMain_CopyBody(entity this, entity body)
{
	body.scale = this.scale;
}

// ForbidWeaponUse mutator hook, returns bool "forbid"
bool MacroMain_ForbidWeaponUse_Forbid(entity this)
{
	// you can't use your weapon if you are too tiny or big
	if(autocvar_g_macro_weapon_use_min && this.scale && this.scale < autocvar_g_macro_weapon_use_min)
		return true;
	else if(autocvar_g_macro_weapon_use_max && this.scale && this.scale > autocvar_g_macro_weapon_use_max)
		return true;

	return false;
}
